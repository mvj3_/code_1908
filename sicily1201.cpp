//题目网址:
//http://soj.me/1201

//题目分析:
//二进制加法，高精度加法，利用数组，从后往前加，记得处理进位，注意000+00情况。
//最后只要输出一个0。

#include <iostream>
#include <string>

using namespace std;

int main()
{
    int N;
    int num = 1;
    int a[82] = {0};
    int b[82] = {0};
    cin >> N;

    while (num <= N) {
        string c1, c2;
        cin >> c1 >> c2;
        int a1, b1;
        a1 = 82;
        b1 = 82;
        bool flag = false;

        for (int i = c2.length() - 1; i >= 0; i--)  { b[--b1 = c2[i] - '0';  }
        for (int i = c1.length() - 1; i >= 0; i--)  { a[--a1] = c1[i] - '0';  }

        int e;
        if (a1 > b1) { e = b1; }
        else { e = a1; }

        int j;
        for (j = 81; j >= e; j--) {
            a[j] += b[j];
            if (flag) {
                a[j]++;
                flag = false;
            } 

            if (a[j] == 2) {
                a[j] = 0;
                flag = true;
            } else if (a[j] == 3) {
                a[j] = 1;
                flag = true;
            }
        }

        if (flag)
            a[j] = 1;

        cout << num << ' ';
        if (a[j] == 0) { j++; flag = true;}
        while (j <= 81) {
            if (a[j] == 0 && flag) {
                b[j] = 0;
                j++;
                continue;
            } else if (a[j] != 0) {
                flag = false;
            }
            cout << a[j];
            a[j] = 0;
            b[j] = 0;
            j++;
        }
        if (flag) { cout << 0; }
        cout << endl;

        num++;
    }
    
    return 0;
}                                 